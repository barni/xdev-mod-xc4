<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\EngineBased\XC4\App\Config\Repo\v4_4_0;

/**
 * Class API
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Repo extends \XDev\Module\XC4\EngineBased\XC4\App\Config\Repo\v4_0_0\Repo
{
    protected function createGitignoreBuilder() {

        $builder = parent::createGitignoreBuilder();

        $builder
            ->section('xc4_allow')
                ->replaceRule('!/skin1/**', '!/skin/**')
        ;

        return $builder;

    }
}
